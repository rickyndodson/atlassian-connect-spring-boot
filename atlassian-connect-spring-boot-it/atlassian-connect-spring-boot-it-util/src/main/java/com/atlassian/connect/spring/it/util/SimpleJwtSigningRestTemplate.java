package com.atlassian.connect.spring.it.util;

import com.atlassian.connect.spring.AtlassianHost;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.client.ClientHttpRequestInterceptor;

import java.security.interfaces.RSAPrivateKey;
import java.util.Map;
import java.util.Optional;

public class SimpleJwtSigningRestTemplate extends TestRestTemplate {

    public static SimpleJwtSigningRestTemplate asymmetricJwtSigningRestTemplate(AtlassianHost host, String appBaseUrl, RSAPrivateKey privateKey, String keyId) {
        return new SimpleJwtSigningRestTemplate(host, appBaseUrl, privateKey, keyId);
    }

    public SimpleJwtSigningRestTemplate(String jwt) {
        this(new FixedJwtSigningClientHttpRequestInterceptor(jwt));
    }

    public SimpleJwtSigningRestTemplate(AtlassianHost host, Optional<String> optionalSubject) {
        this(new AtlassianHostJwtSigningClientHttpRequestInterceptor(host, optionalSubject));
    }

    public SimpleJwtSigningRestTemplate(AtlassianHost host, Optional<String> optionalSubject, Optional<Map<String, Object>> context) {
        this(new AtlassianHostJwtSigningClientHttpRequestInterceptor(host, optionalSubject, context));
    }

    protected SimpleJwtSigningRestTemplate(AtlassianHost host, String appBaseUrl, RSAPrivateKey privateKey, String keyId) {
        this(new AsymmetricJwtSigningClientHttpRequestInterceptor(host.getClientKey(), appBaseUrl, privateKey, keyId, Optional.empty(), Optional.empty()));
    }

    public SimpleJwtSigningRestTemplate(ClientHttpRequestInterceptor requestInterceptor) {
        super(new RestTemplateBuilder().additionalInterceptors(requestInterceptor));
    }

    public SimpleJwtSigningRestTemplate(RestTemplateBuilder restTemplateBuilder, AtlassianHost host, String appBaseUrl, RSAPrivateKey privateKey, String keyId) {
        super(restTemplateBuilder.additionalInterceptors(new AsymmetricJwtSigningClientHttpRequestInterceptor(host.getClientKey(), appBaseUrl, privateKey, keyId, Optional.empty(), Optional.empty())));
    }
}
