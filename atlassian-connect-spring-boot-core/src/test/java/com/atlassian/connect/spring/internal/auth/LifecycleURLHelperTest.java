package com.atlassian.connect.spring.internal.auth;

import com.atlassian.connect.spring.internal.descriptor.AddonDescriptor;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.when;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class LifecycleURLHelperTest {
    @Mock
    AddonDescriptorLoader addonDescriptorLoader;

    @InjectMocks
    LifecycleURLHelper lifecycleURLHelper;

    HttpServletRequest request;

    AddonDescriptor addonDescriptor;

    @Before
    public void init() {
        addonDescriptor = mock(AddonDescriptor.class);
        request = mock(HttpServletRequest.class);
        when(addonDescriptor.getBaseUrl()).thenReturn("https://example.com/");
        when(addonDescriptor.getInstalledLifecycleUrl()).thenReturn("/installed");
        when(addonDescriptor.getUninstalledLifecycleUrl()).thenReturn("/uninstalled");
        when(addonDescriptorLoader.getDescriptor()).thenReturn(addonDescriptor);
    }

    @Test
    public void shouldReturnTrueWithMatchingInstallUrlPath() {
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/installed"));
        assertTrue(lifecycleURLHelper.isRequestToInstalledLifecycle(request));
    }

    @Test
    public void shouldReturnFalseWithNotMatchingInstallUrlPath() {
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/different_path"));
        assertFalse(lifecycleURLHelper.isRequestToInstalledLifecycle(request));
    }

    @Test
    public void shouldReturnTrueWithMatchingUninstallUrlPath() {
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/uninstalled"));
        assertTrue(lifecycleURLHelper.isRequestToUninstalledLifecycle(request));
    }

    @Test
    public void shouldReturnFalseWithNotMatchingUninstallUrlPath() {
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/different_path"));
        assertFalse(lifecycleURLHelper.isRequestToUninstalledLifecycle(request));
    }

    @Test
    public void shouldReturnFalseIfInstallUrlIsNotDefined() {
        when(addonDescriptor.getInstalledLifecycleUrl()).thenReturn(null);
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/installed"));
        assertFalse(lifecycleURLHelper.isRequestToInstalledLifecycle(request));
    }

    @Test
    public void shouldReturnFalseIfUninstallUrlIsNotDefined() {
        when(addonDescriptor.getUninstalledLifecycleUrl()).thenReturn(null);
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/uninstalled"));
        assertFalse(lifecycleURLHelper.isRequestToUninstalledLifecycle(request));
    }

    @Test
    public void isRequestToLifecycleURLShouldReturnTrueWhenInstallLifecycleUrlMatches() {
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/installed"));
        assertTrue(lifecycleURLHelper.isRequestToLifecycleURL(request));
    }

    @Test
    public void isRequestToLifecycleURLShouldReturnTrueWhenUninstallLifecycleUrlMatches() {
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/uninstalled"));
        assertTrue(lifecycleURLHelper.isRequestToLifecycleURL(request));
    }

    @Test
    public void isRequestToLifecycleURLShouldReturnFalseWhenUrlIsNotALifecycleOne() {
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/normal"));
        assertFalse(lifecycleURLHelper.isRequestToLifecycleURL(request));
    }

    @Test
    public void isRequestToLifecycleURLShouldReturnFalseWhenUrlIsEmpty() {
        when(request.getRequestURL()).thenReturn(new StringBuffer(""));
        assertFalse(lifecycleURLHelper.isRequestToLifecycleURL(request));
    }
}
