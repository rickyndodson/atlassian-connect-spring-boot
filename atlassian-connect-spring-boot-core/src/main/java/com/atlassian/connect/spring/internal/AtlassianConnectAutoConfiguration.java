package com.atlassian.connect.spring.internal;

import com.atlassian.connect.spring.internal.auth.LifecycleURLHelper;
import com.atlassian.connect.spring.internal.auth.asymmetric.AsymmetricAuthenticationFilter;
import com.atlassian.connect.spring.internal.auth.jwt.JwtAuthenticationFilter;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.authentication.AuthenticationManager;

/**
 * {@link EnableAutoConfiguration Auto-configuration} for Atlassian Connect add-ons.
 */
@Configuration
@ComponentScan(basePackageClasses = {AtlassianConnectAutoConfiguration.class})
@EnableConfigurationProperties(AtlassianConnectProperties.class)
@ConditionalOnResource(resources = AddonDescriptorLoader.DESCRIPTOR_RESOURCE_PATH)
@EnableCaching
@EnableAsync
public class AtlassianConnectAutoConfiguration {

    @Bean
    public FilterRegistrationBean jwtAuthenticationFilterRegistrationBean(AuthenticationManager authenticationManager,
                                                                          AddonDescriptorLoader addonDescriptorLoader,
                                                                          AtlassianConnectProperties atlassianConnectProperties,
                                                                          ServerProperties serverProperties,
                                                                          LifecycleURLHelper lifecycleURLHelper) {
        FilterRegistrationBean<JwtAuthenticationFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new JwtAuthenticationFilter(authenticationManager, addonDescriptorLoader,
                atlassianConnectProperties, serverProperties, lifecycleURLHelper));
        registrationBean.setOrder(atlassianConnectProperties.getJwtFilterOrder());
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean asymmetricAuthenticationFilterRegistrationBean(AuthenticationManager authenticationManager,
                                                                                 AddonDescriptorLoader addonDescriptorLoader,
                                                                                 ServerProperties serverProperties,
                                                                                 AtlassianConnectProperties atlassianConnectProperties,
                                                                                 LifecycleURLHelper lifecycleURLHelper) {
        FilterRegistrationBean<AsymmetricAuthenticationFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new AsymmetricAuthenticationFilter(authenticationManager, addonDescriptorLoader,
                serverProperties, lifecycleURLHelper, atlassianConnectProperties));
        registrationBean.setOrder(atlassianConnectProperties.getAsymmetricAuthFilterOrder());
        return registrationBean;
    }

    @Configuration
    @PropertySource("classpath:config/default.properties")
    static class Defaults {
    }

    @Configuration
    @Profile("production")
    @PropertySource({"classpath:config/default.properties", "classpath:config/production.properties"})
    static class Production {
    }
}
