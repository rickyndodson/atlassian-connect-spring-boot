package com.atlassian.connect.spring.internal.jwt;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/**
 * Tests aspects of the http request canonicalizer
 */
@RunWith(MockitoJUnitRunner.class)
public class CanonicalizerTest {
    @Mock
    private CanonicalHttpRequest request;

    @Before
    public void setUp() throws Exception {
        // Some servlet containers may return an array with a null value for a parameter
        Map<String, String[]> mapWithNulls = new HashMap<>();
        mapWithNulls.put("p1", new String[]{"value"});
        mapWithNulls.put("p2", new String[]{null});
        mapWithNulls.put("p3", new String[]{""});
        mapWithNulls.put("p4", new String[]{"other"});
        mapWithNulls.put("jwt", new String[]{"fake-jwt"});
        when(request.getParameterMap()).thenReturn(mapWithNulls);

        when(request.getRelativePath()).thenReturn("/path");
        when(request.getMethod()).thenReturn("GET");
    }

    /**
     * Test that the toVerboseString method can handle a parameter map with null entries
     */
    @Test
    public void testVerboseStringWithNullParameterMap() {
        try {
            CanonicalRequestUtil.toVerboseString(request);
        } catch (Exception e) {
            fail("VerboseString threw unexpected exception: " + e.getClass().getCanonicalName());
        }
    }

    /**
     * Test that canonicalizing the request can handle null entries in the parameter map
     */
    @Test
    public void testCanonicaliseQueryParameters() throws UnsupportedEncodingException {
        String result = HttpRequestCanonicalizer.canonicalize(request);
        assertEquals("Canonicalised request incorrect",
                "GET&/path&p1=value&p2=&p3=&p4=other", result);
    }
}
