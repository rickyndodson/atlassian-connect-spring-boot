package com.atlassian.connect.spring;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * A <a href="http://projects.spring.io/spring-data/">Spring Data</a> repository for information about Atlassian hosts
 * in which the add-on is or has been installed.
 *
 * <p><strong>NOTE:</strong> A component implementing this interface is required for your application to start. Such a
 * component is typically obtained by
 * <a href="https://docs.spring.io/spring-data/data-commons/docs/current/reference/html/#repositories.create-instances.java-config">including the appropriate {@code @Enable${store}Repositories} annotation</a>.
 *
 * <h3>Spring Data JPA Support</h3>
 *
 * <p>For using <a href="http://projects.spring.io/spring-data-jpa/">Spring Data JPA</a>, instead of including the
 * {@code @EnableJpaRepositories} annotation yourself, include the following dependency on the companion Spring Boot
 * starter in your Maven POM, which additionally provides integration with
 * <a href="http://www.liquibase.org/">Liquibase</a> and enables <a href="https://docs.spring.io/spring-data/data-commons/docs/current/reference/html/#auditing">Spring Data auditing</a>:
 * <blockquote><pre><code> &lt;dependency&gt;
 *     &lt;groupId&gt;com.atlassian.connect&lt;/groupId&gt;
 *     &lt;artifactId&gt;atlassian-connect-spring-boot-jpa-starter&lt;/artifactId&gt;
 *     &lt;version&gt;${atlassian-connect-spring-boot.version}&lt;/version&gt;
 * &lt;/dependency&gt;</code></pre></blockquote>
 *
 * @since 1.0.0
 */
public interface AtlassianHostRepository extends CrudRepository<AtlassianHost, String> {

    /**
     * Returns the host with the given base URL.
     *
     * @deprecated No replacement. Look up by {@link AtlassianHost#getClientKey()} using {@link #findById} 
     * @param baseUrl the base URL of the host application
     * @return the host with the given base URL or {@link Optional#empty()}
     * @see AtlassianHost#getBaseUrl()
     */
    @Deprecated
    Optional<AtlassianHost> findFirstByBaseUrl(String baseUrl);

    /**
     * Returns the host with the given base URL. If multiple hosts are found, the most recently modified host is
     * returned.
     *
     * @deprecated No replacement. Look up by {@link AtlassianHost#getClientKey()} using {@link #findById}
     * @param baseUrl the base URL of the host application
     * @return the host with the given base URL or {@link Optional#empty()}
     * @see AtlassianHost#getBaseUrl()
     */
    @Deprecated
    Optional<AtlassianHost> findFirstByBaseUrlOrderByLastModifiedDateDesc(String baseUrl);
}
