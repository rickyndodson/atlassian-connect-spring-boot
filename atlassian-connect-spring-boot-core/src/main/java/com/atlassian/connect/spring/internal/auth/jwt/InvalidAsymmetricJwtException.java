package com.atlassian.connect.spring.internal.auth.jwt;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * An authentication exception thrown when Asymmetric JWT authentication failed during install/uninstall lifecycle
 */
@ResponseStatus(code = HttpStatus.UNAUTHORIZED, reason = "Invalid JWT for install hook")
public class InvalidAsymmetricJwtException extends AuthenticationException {
    public InvalidAsymmetricJwtException(String message) {
        super(message);
    }
}
