package com.atlassian.connect.spring.internal.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.endpoint.web.WebEndpointProperties;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter.ReferrerPolicy;

import java.util.concurrent.TimeUnit;

@Configuration
@Order(SecurityProperties.BASIC_AUTH_ORDER + 1)
public class AtlassianConnectWebSecurityConfigurer extends WebSecurityConfigurerAdapter {

    private static final long ONE_YEAR_IN_SECONDS = TimeUnit.SECONDS.convert(365, TimeUnit.DAYS);

    @Autowired
    private WebEndpointProperties webEndpointProperties;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers()
                .frameOptions().disable()
                .httpStrictTransportSecurity().includeSubDomains(false).maxAgeInSeconds(ONE_YEAR_IN_SECONDS).and()
                .referrerPolicy().policy(ReferrerPolicy.ORIGIN_WHEN_CROSS_ORIGIN);
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        String managementPath = webEndpointProperties.getBasePath();
        if (managementPath != null) {
            http.authorizeRequests().antMatchers(managementPath + "/**").authenticated().and()
                    .httpBasic();
        }
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
