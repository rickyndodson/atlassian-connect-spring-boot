package com.atlassian.connect.spring.it.auth;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestTemplateCacheIT extends BaseApplicationIT {

    @Autowired
    private AtlassianHostRestClients atlassianHostRestClients;

    @Test
    public void shouldRetrieveOAuth2ClientWithCaching() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        AtlassianHostUser hostUser = AtlassianHostUser.builder(host).withUserAccountId("some-account-id").build();

        assertThat(this.atlassianHostRestClients.authenticatedAs(hostUser), is(notNullValue()));
    }
}
