package com.atlassian.connect.spring.internal.auth.asymmetric;

import com.atlassian.connect.spring.internal.auth.jwt.JwtCredentials;
import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;

public class AsymmetricCredentials extends JwtCredentials {

    public AsymmetricCredentials(String rawJWT, CanonicalHttpRequest canonicalHttpRequest) {
        super(rawJWT, canonicalHttpRequest);
    }
}
