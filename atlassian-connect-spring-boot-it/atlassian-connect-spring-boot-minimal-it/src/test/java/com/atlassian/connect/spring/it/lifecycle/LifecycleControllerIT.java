package com.atlassian.connect.spring.it.lifecycle;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.BASE_URL;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.CLIENT_KEY;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.OTHER_SHARED_SECRET;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.SHARED_SECRET;
import static com.atlassian.connect.spring.it.util.LifecycleBodyHelper.createLifecycleJson;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LifecycleControllerIT extends BaseLifecycleControllerIT {
    private static final String USER_ACCOUNT_ID = "123:ACCID";

    @Test
    public void shouldStoreHostOnFirstInstall() throws Exception {
        setJwtAuthenticatedPrincipal(new AtlassianHostBuilder().clientKey(CLIENT_KEY).build(), USER_ACCOUNT_ID);

        mvc.perform(postInstalled("/installed", SHARED_SECRET))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getSharedSecret(), is(SHARED_SECRET));
        assertThat(installedHost.getBaseUrl(), is(BASE_URL));
        assertThat(installedHost.isAddonInstalled(), is(true));

        assertThat(installedHost.getCreatedDate(), is(notNullValue()));
        assertThat(installedHost.getLastModifiedDate(), is(notNullValue()));
        assertThat(installedHost.getCreatedBy(), equalTo(USER_ACCOUNT_ID));
        assertThat(installedHost.getLastModifiedBy(), equalTo(USER_ACCOUNT_ID));
    }

    @Test
    public void shouldUpdateSharedSecretOnSignedSecondInstall() throws Exception {
        AtlassianHost host = saveHost(CLIENT_KEY, SHARED_SECRET, BASE_URL);
        Calendar creationDate = host.getCreatedDate();
        Calendar creationLastModified = host.getLastModifiedDate();

        String newSharedSecret = "some-other-secret";
        setJwtAuthenticatedPrincipal(host, USER_ACCOUNT_ID);
        mvc.perform(postInstalled("/installed", newSharedSecret))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getSharedSecret(), is(newSharedSecret));

        assertThat(installedHost.getCreatedDate(), is(notNullValue()));
        assertThat(installedHost.getLastModifiedDate(), is(notNullValue()));
        assertThat(installedHost.getCreatedBy(), equalTo(USER_ACCOUNT_ID));
        assertThat(installedHost.getLastModifiedBy(), equalTo(USER_ACCOUNT_ID));

        assertEquals("Creation date should not change", creationDate, installedHost.getCreatedDate());
        assertNotEquals("Last Modified date should be updated", creationLastModified, installedHost.getLastModifiedDate());
    }

    @Test
    public void shouldRejectSecondInstallWithoutJwt() throws Exception {
        saveHost(CLIENT_KEY, SHARED_SECRET, BASE_URL);
        mvc.perform(postInstalled("/installed", SHARED_SECRET))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldRejectSharedSecretUpdateByOtherHost() throws Exception {
        saveHost(CLIENT_KEY, SHARED_SECRET, BASE_URL);
        AtlassianHost otherHost = saveHost("other-host", OTHER_SHARED_SECRET, "http://other-example.com");
        setJwtAuthenticatedPrincipal(otherHost);
        mvc.perform(postInstalled("/installed", "some-other-secret"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void shouldRejectUninstallByOtherHost() throws Exception {
        saveHost(CLIENT_KEY, SHARED_SECRET, BASE_URL);
        AtlassianHost otherHost = saveHost("other-host", OTHER_SHARED_SECRET, "http://other-example.com");
        setJwtAuthenticatedPrincipal(otherHost);
        mvc.perform(postUninstalled("/uninstalled", SHARED_SECRET))
                .andExpect(status().isForbidden());
    }

    @Test
    public void shouldSoftDeleteHostOnUninstall() throws Exception {
        AtlassianHost host = saveHost(CLIENT_KEY, SHARED_SECRET, BASE_URL);
        setJwtAuthenticatedPrincipal(host, USER_ACCOUNT_ID);
        mvc.perform(postUninstalled("/uninstalled", SHARED_SECRET))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getSharedSecret(), is(SHARED_SECRET));
        assertThat(installedHost.getBaseUrl(), is(BASE_URL));
        assertThat(installedHost.isAddonInstalled(), is(false));

        assertThat(installedHost.getCreatedDate(), is(notNullValue()));
        assertThat(installedHost.getLastModifiedDate(), is(notNullValue()));
        assertThat(installedHost.getCreatedBy(), equalTo(USER_ACCOUNT_ID));
        assertThat(installedHost.getLastModifiedBy(), equalTo(USER_ACCOUNT_ID));
    }

    @Test
    public void shouldIgnoreMissingHostOnUninstall() throws Exception {
        mvc.perform(postUninstalled("/uninstalled", SHARED_SECRET))
                .andExpect(status().isNoContent());

        Optional<AtlassianHost> installedHost = hostRepository.findById(CLIENT_KEY);
        assertThat(installedHost, is(Optional.empty()));
    }

    @Test
    public void shouldRejectInstallWithoutBody() throws Exception {
        mvc.perform(post("/installed")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldRejectInstallWithInvalidBody() throws Exception {
        mvc.perform(post("/installed")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldRejectInstallWithInvalidEventType() throws Exception {
        mvc.perform(post("/installed")
                .contentType(MediaType.APPLICATION_JSON)
                .content(createLifecycleJson("uninstalled", "some-secret")))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldRejectUninstallWithoutBody() throws Exception {
        mvc.perform(post("/uninstalled")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldRejectUninstallWithInvalidBody() throws Exception {
        mvc.perform(post("/uninstalled")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldRejectUninstallWithInvalidEventType() throws Exception {
        mvc.perform(post("/uninstalled")
                .contentType(MediaType.APPLICATION_JSON)
                .content(createLifecycleJson("installed", "some-secret")))
                .andExpect(status().isBadRequest());
    }

    private AtlassianHost saveHost(String clientKey, String sharedSecret, String baseUrl) {
        AtlassianHost host = new AtlassianHost();
        host.setClientKey(clientKey);
        host.setSharedSecret(sharedSecret);
        host.setBaseUrl(baseUrl);
        host.setCreatedBy(USER_ACCOUNT_ID);
        host.setLastModifiedBy(USER_ACCOUNT_ID);
        host.setCreatedDate(new GregorianCalendar());
        host.setLastModifiedDate(new GregorianCalendar());
        host = hostRepository.save(host);

        assertThat(host.getCreatedDate(), is(notNullValue()));
        assertThat(host.getLastModifiedDate(), is(notNullValue()));
        assertThat(host.getCreatedBy(), equalTo(USER_ACCOUNT_ID));
        assertThat(host.getLastModifiedBy(), equalTo(USER_ACCOUNT_ID));
        return host;
    }
}
